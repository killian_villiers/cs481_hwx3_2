﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace homework3
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("About page alert", "Welcome", "Go");
        }

        void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            DisplayAlert("About page alert", "Bye", "Go");

        }
    }
}
